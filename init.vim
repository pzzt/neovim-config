:set number
" :set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
:set mouse=a

:set termguicolors

:set background=dark
" :colorscheme solarized8_flat
autocmd vimenter * ++nested colorscheme solarized8_flat
" autocmd vimenter * ++nested colorscheme nord

call plug#begin()

Plug 'https://github.com/vim-airline/vim-airline' " Barra di stato
Plug 'https://github.com/preservim/nerdtree' " NerdTree
Plug 'mattn/emmet-vim' " CSS&HTML autocomplete
Plug 'scrooloose/syntastic' "Syntax checking hacks for vim
Plug 'majutsushi/tagbar' " Vim plugin that displays tags in a window, ordered by scope
" Plug 'jceb/vim-orgmode' "orgmode per Vim
Plug 'bbrtj/vim-vorg-md' "Simile a orgmode emacs, non supporta i file org di emacs

Plug 'voldikss/vim-floaterm' " Vim Floaterm is a plugin that allows you to use the Vim 8 terminal feature as a popup window.



" Temi
Plug 'arcticicestudio/nord-vim' " Tema Nord
Plug 'https://github.com/lifepillar/vim-solarized8' " Solarized8
call plug#end()
