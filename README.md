# Neovim Config file

Prima installa Vim Plug https://github.com/junegunn/vim-plug

### Plugins:
- vim-airline/vim-airline Barra di stato
- preservim/nerdtree NerdTree
- mattn/emmet-vim CSS&HTML autocomplete
- scrooloose/syntastic Syntax checking hacks for vim
- majutsushi/tagbar  Vim plugin that displays tags in a window, ordered by scope
- bbrtj/vim-vorg-md Simile a orgmode emacs, non supporta i file org di emacs
- voldikss/vim-floaterm  Vim Floaterm is a plugin that allows you to use the Vim 8 terminal feature as a popup window.

### Temi:
- arcticicestudio/nord-vim Tema Nord
- lifepillar/vim-solarized8  Solarized8
